from os.path import isfile

from api.server import run

from api.database.database_provider import DatabaseProvider


def main():
    init_db()
    run()


def init_db():
    if (isfile('database.db')):
        print('Database exists, skipping database creation...')
        return
    print('Database not found, creating one...')

    db_provider = DatabaseProvider()

    connection = db_provider.get_connection()
    cursor = connection.cursor()

    cursor.execute('''
        CREATE TABLE Ads(
        ad_id INTEGER PRIMARY KEY,
        ad_title text NOT NULL,
        description text NOT NULL,
        cover_image_url text NOT NULL,
        price real NOT NULL,
        created_at
        DATETIME NOT NULL  DEFAULT (strftime('%d/%m/%Y %H:%M:%S', 'NOW')))
    ''')

    cursor.execute('''
        CREATE TABLE Images(
        image_id INTEGER PRIMARY KEY,
        image_url text NOT NULL,
        ad_id integer NOT NULL,
        FOREIGN KEY(ad_id) REFERENCES Ads(ad_id))
    ''')

    connection.commit()

    connection.close()


if __name__ == "__main__":
    main()
