from http.server import BaseHTTPRequestHandler

from api.database.database_proxy import get_ads, get_ad_by_id, create_ad

from api.database.database_provider import DatabaseProvider

from urllib.parse import urlsplit, parse_qs

from api.enums.sort_order import SortOrder

from api.enums.error_code import ErrorCode

from json import loads


class RequestHandler(BaseHTTPRequestHandler):

    def __init__(self, *args):
        self.db_provider = DatabaseProvider()
        BaseHTTPRequestHandler.__init__(self, *args)

    def __isnumericparam(self, param):
        return (len(param) == 1) and (param[0].isnumeric())

    def __set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def do_HEAD(self):
        self.__set_headers()

    def do_GET(self):
        url = urlsplit(self.path)
        query = url.query
        params = parse_qs(query)

        if url.path == '/':
            page = 1
            if ('page' in params):
                page_param = params.get('page')
                if (self.__isnumericparam(page_param)):
                    page = page_param[0]
                else:
                    self.send_error(400, str(ErrorCode.PageParamNotValid))
                    return

            price_sort = SortOrder.DESC
            if ('pricesort' in params):
                price_sort = SortOrder.getByString(params.get('pricesort')[0])

            date_sort = SortOrder.DESC
            if ('datesort' in params):
                date_sort = SortOrder.getByString(params.get('datesort')[0])

            ads = get_ads(
                self.db_provider.get_connection(),
                int(page),
                price_sort,
                date_sort)

            if (ads is None):
                self.send_error(404, str(ErrorCode.EmptyPage))
                return

            self.__set_headers()
            self.wfile.write(ads)

        elif url.path == '/ad' and 'id' in params:
            id = params.get('id')
            if self.__isnumericparam(id):
                ad = get_ad_by_id(
                    self.db_provider.get_connection(),
                    int(id[0]))
                if (ad is None):
                    self.send_error(404, str(ErrorCode.AdNotFound))
                    return
                self.__set_headers()
                self.wfile.write(ad)
            else:
                self.send_error(400, str(ErrorCode.AdInvalidId))
                return
        else:
            self.send_response(404)

    def do_POST(self):

        def __exists(item):
            return item is not None

        content_length = int(self.headers.get('Content-Length'))
        body = self.rfile.read(content_length)
        request_body_json = loads(body.decode('utf-8'))

        ad_title = request_body_json.get('ad_title')
        description = request_body_json.get('description')
        cover_image_url = request_body_json.get('cover_image_url')
        price = request_body_json.get('price')
        images = request_body_json.get('images')

        for item in (
                ('ad_title', ad_title),
                ('description', description),
                ('cover_image_url', cover_image_url),
                ('price', price),
                ('images', images)):
            if (not __exists(item[1])):
                self.send_error(400, str(ErrorCode.ParamNotGiven) % item[0])
                return

        id = create_ad(self.db_provider.get_connection(), ad_title,
                       description, cover_image_url, price, images)

        if (id is not None):
            self.__set_headers()
            self.wfile.write(str(id).encode('utf8'))
            self.wfile.write(str('\r\n200 OK').encode('utf8'))
        else:
            self.send_error(400, str(ErrorCode.AdCreateError))
