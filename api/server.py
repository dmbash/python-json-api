from http.server import HTTPServer
from api.api import RequestHandler


def run(server_address=('localhost', 8000),
        http_server=HTTPServer,
        request_handler=RequestHandler):
    httpd = http_server(server_address, request_handler)
    print('Serving at ' +
          str(server_address[0]) + ':' + str(server_address[1]) + '...')
    httpd.serve_forever()
